$(function() {
  $( "#tabs" ).tabs();
} );
$(function () {
  $("#accordion").accordion({
      collapsible: true,
  });
});
function accordionSvg(el) {
  $('.accordion-header_svg').attr('src', './../../static/img/accordeon_plus.svg')
  setTimeout(function () {
      if ($(".ui-accordion-header").hasClass("ui-accordion-header-active")) {
          $(el).find('.accordion-header_svg').attr('src', './../../static/img/accordeon_minus.svg')
      }
  }, 100)
};

function tabsSvgChangeColor(elTab){
  $('.tab-header__warning-ico').attr('src', './../../static/img/icons/warning.svg')
  if ($(".ui-tabs-tab").hasClass("ui-tabs-active")) {
      $(elTab).find('.tab-header__warning-ico').attr('src', './../../static/img/icons/warning-white.svg')
  }
}
function hideHeader(elInput){
    if (window.innerWidth <= 768) {
        $("body").addClass("hide_header");
        $(".body").addClass("hide_header_body__no-scroll");
        
    }
}
function showHeader(elInput){
    if (window.innerWidth <= 768) {
        $("body").removeClass("hide_header");
        $(".body").removeClass("hide_header_body__no-scroll");
    }
}

$(document).ready(function() {
  if ($(".ui-accordion-content-active").is(".task-content__warnings")) {
      $(".accordion-header_svg-warning").addClass("svg-warning-active");
  }
});