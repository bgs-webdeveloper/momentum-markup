(function () {
  'use strict';

  window.addEventListener('load', function () {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');

    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function (form) {
      form.addEventListener('submit', function (event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();

function openSidebar() {
  var el = document.getElementById('sidebar')
  var isOpen = el.classList.contains("open")

  if (!isOpen) el.classList.add('open')
}

function closeSidebar(e) {
  // var targerEl = e.target.classList.contains('js_open-sidebar', 'sidebar')
  // var el = document.getElementById('sidebar')
  // var isOpen = el.classList.contains("open")

  // if (isOpen && !targerEl) el.classList.remove('open')
  document.getElementById('sidebar').classList.remove('open')
}

if (!!document.querySelector('.js_open-sidebar')) {
  // document.addEventListener('click', closeSidebar);
  document.querySelector('.js_open-sidebar').addEventListener('click', openSidebar);
  document.querySelector('.js_close-sidebar').addEventListener('click', closeSidebar);
};

function getHeightContentBlock() {
  var height = 0
  var heightWarningsBlock = 0
  var heightHeaderStatus = 0
  var heightTaskHead = 0
  var heightFooter = 0

  if($('.header-status').length) {
    heightHeaderStatus = $('.header-status').height()
  }

  if($('.task-content__head').length) {
    heightTaskHead = $('.task-content__head').height()
  }

  if($('.footer').length) {
    heightFooter = $('.footer').height()
  }

  if($('.table').length) {
    height = $('.js_content-block .table').height()
  }
  if($('.task-content__body__content').length) {
    height = $('.task-content__body__content').height()
  }
  if($('.task-content__warnings').length) {
    heightWarningsBlock = $('.task-content__warnings').height()
  }

  if(window.innerHeight < height + heightHeaderStatus + heightTaskHead + heightFooter + heightWarningsBlock) {
    $('.js_content-block').addClass('shadow-block')
  } else {
    $('.js_content-block').removeClass('shadow-block')
  }
};

var wx = window.innerWidth;
var wy = window.innerHeight;

$(window).resize(function() {
  wx = window.innerWidth;
  wy = window.innerHeight;

  blockedButtonCameraScan()
  footerActionBar()
})

function blockedButtonCameraScan() {
  if (wx <= 667 && wy <= 400) {
    $('.js_popup-scan_show').addClass('disabled')
  } else {
    $('.js_popup-scan_show').removeClass('disabled')
  }
}
blockedButtonCameraScan();


function footerActionBar() {
  console.log(wx < 666, $('.js_footer-buttons').children('button').length > 5)
  if (wx < 666 && $('.js_footer-buttons').children('button').length > 5) {
    $('.js_action-bar_show').addClass('active')
    $('.js_footer-buttons').addClass('active')
  } else {
    console.log('remove')
    $('.js_action-bar_show').removeClass('active')
    $('.js_footer-buttons').removeClass('active')
  }
}
footerActionBar()

$('.js_action-bar_show').on('click', function() {
  $('.js_action-bar_show').toggleClass('more')
  $('.js_footer-buttons').toggleClass('more');
})

$('.js_popup-scan_show').on('click', function() {
  if (!$(this).hasClass('disabled')) {
    $('.js_popup-scan').addClass('active')
  }
})
$('.js_popup-scan_close').on('click', function() {
  $('.js_popup-scan').removeClass('active')
})

$('.js_header-status_complete').on('click', function() {
  $(this).addClass('complete')
  setTimeout(function(){
      $(this).removeClass('complete')
      setTimeout(function(){
        $(this).addClass('error')
      }.bind(this),1000)
  }.bind(this), 5000)
})

/* $('.js_select select').on('click', function () {
  console.log('focus')
  $(this).closest('.js_select').toggleClass('active')
}) */

$('input').on('focus', function() {
  $('body').addClass('mobile-footer-off');
  /* setTimeout(function () {
    $(window).resize(function() {
      removeBodyClass()
    })
  }, 2000) */
}) 

$('input').on('blur', function() {
  $('body').removeClass('mobile-footer-off');
})

function removeBodyClass() {
  $('body').removeClass('mobile-footer-off')
}

$(document).ready(function() {
  var windowHeight = window.innerHeight
  $('.body').css({height: windowHeight})
  getHeightContentBlock()
})

$(window).resize(function() {
  getHeightContentBlock()

  // $('body').removeClass('mobile-footer-off')
})