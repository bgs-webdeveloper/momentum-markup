// variables
var scanCode = '';
var scanBarCode = '';
var isSealedScanCode = false;
var isVisibleKeyboard = false;
var selectedDeviceId;
var codeReader = undefined;
var errorScanCode = '';
var isOpenCameraScan = false;
var isSuccessCameraScan = false;
var mql = window.matchMedia("(orientation: portrait)");
var isActiveFooterBtn = false;
var isBarCodeScan = false;
var isKeyDown = false;
var isShift = false;
var wx = window.innerWidth;
var wy = window.innerHeight;
var isUseEnglishKeyboard = false;
var isSingleInputFocus = false;
var realHeight = 0;

var charStandart = new Map([
    [9, ' '],
    [32, ' '],
    [48, '0'],
    [49, '1'],
    [50, '2'],
    [51, '3'],
    [52, '4'],
    [53, '5'],
    [54, '6'],
    [55, '7'],
    [56, '8'],
    [57, '9'],
    [65, 'a'],
    [66, 'b'],
    [67, 'c'],
    [68, 'd'],
    [69, 'e'],
    [70, 'f'],
    [71, 'g'],
    [72, 'h'],
    [73, 'i'],
    [74, 'j'],
    [75, 'k'],
    [76, 'l'],
    [77, 'm'],
    [78, 'n'],
    [79, 'o'],
    [80, 'p'],
    [81, 'q'],
    [82, 'r'],
    [83, 's'],
    [84, 't'],
    [85, 'u'],
    [86, 'v'],
    [87, 'w'],
    [88, 'x'],
    [89, 'y'],
    [90, 'z'],
    [95, '_'],
    [96, '0'],
    [97, '1'],
    [98, '2'],
    [99, '3'],
    [100, '4'],
    [101, '5'],
    [102, '6'],
    [103, '7'],
    [104, '8'],
    [105, '9'],
    [106, '*'],
    [107, '+'],
    [109, '-'],
    [186, ';'],
    [187, '='],
    [188, ','],
    [189, '-'],
    [190, '.'],
    [191, '/'],
    [219, '['],
    [220, '\\'],
    [221, ']'],
    [222, '\''],
]);

var charShift = new Map([
    [48, ')'],
    [49, '!'],
    [50, '@'],
    [51, '#'],
    [52, '$'],
    [53, '%'],
    [54, '^'],
    [55, '&'],
    [56, '*'],
    [57, '('],
    [186, ':'],
    [187, '+'],
    [188, '<'],
    [189, '_'],
    [190, '>'],
    [191, '?'],
    [219, '{'],
    [220, '|'],
    [221, '}'],
    [222, '"'],
]);

// functions
window.freeMovementObj = {
    cancelFreeMovementTimeOut: function (timeout) {
        var btn = $('#btnCancelFreeMovement');
        if (btn && btn.length > 0) {
            setTimeout(function () {
                btn.trigger('click');
            }, timeout);
        }
    }
};

window.pickingTaskObj = {
    goToNextTaskTimeOut: function (timeout) {
        var btn = $('#btnNextPickTask');
        if (btn && btn.length > 0) {
            setTimeout(function () {
                btn.trigger('click');
            }, timeout);
        } else {
            btn = $('#btnPreviosPickTask');
            if (btn && btn.length > 0) {
                setTimeout(function () {
                    btn.trigger('click');
                }, timeout);
            }
        }
    }
};

window.modalScanObj = {
    getScanCode: function () {
        var _scanCode = isBarCodeScan ? scanBarCode.trim() : scanCode.trim();
        modalScanObj.cancelScanCode();
        return _scanCode;
    },
    openSimulateScan: function () {
        isSealedScanCode = true;
        isBarCodeScan = false;
        scanCode = '';
        setTimeout(function () {
            document.getElementById("inputValue").focus();
        }, 1200);
    },
    getScanCodeError: function () {
        var _errorScanCode = errorScanCode;
        errorScanCode = '';
        isSealedScanCode = false;
        return _errorScanCode;
    },
    getCameraScanCode: function () {
        var _scanCode = scanCode;
        scanCode = '';
        return _scanCode;
    },
    getCameraScanCodeError: function () {
        var _errorScanCode = errorScanCode;
        errorScanCode = '';
        return _errorScanCode;
    },
    resetScanCode: function () {
        scanCode = '';
        scanBarCode = '';
        isSealedScanCode = false;
        isBarCodeScan = false;
    },
    cancelScanCode: function () {
        setTimeout(function () {
            document.getElementById("inputValue").blur();
            modalScanObj.resetScanCode();
            setTimeout(function () {
                if (!$(".task-content").first().is(':focus')) {
                    $('.task-content').first().focus();
                }
            }, 500);
        }, 700);
    }
};

window.cameraScanObj = {
    init: function () {
        try {
            codeReader = new ZXing.BrowserMultiFormatReader();
            codeReader.getVideoInputDevices().then(function (videoInputDevices) {
                selectedDeviceId = undefined;
                for (var i = 0; i < videoInputDevices.length; i++) {
                    if (videoInputDevices[i].label.includes('back')) {
                        selectedDeviceId = videoInputDevices[i].deviceId;
                        break;
                    }
                }
            }).catch(function (err) {
                errorScanCode = err.message;
            });
        } catch (e) {
            errorScanCode = e.message;
        }

    },
    start: function () {
        scanCode = '';
        isSealedScanCode = true;
        if (codeReader === undefined) {
            this.init();
        }
        if (errorScanCode === '') {
            codeReader.decodeOnceFromVideoDevice(selectedDeviceId || undefined, 'videoCameraScan').then(function (result) {
                scanCode = '';
                scanCode = result.text;
                document.getElementById("btnSimulateCameraScan").click();
            }).catch(function (err) {
                if (isOpenCameraScan) {
                    errorScanCode = err.message;
                    document.getElementById("btnSimulateCameraScanError").click();
                }
            });
        } else {
            document.getElementById("btnSimulateCameraScanError").click();
        }
        isSuccessCameraScan = false;
    },
    stop: function () {
        try {
            isSealedScanCode = false;
            codeReader.videoEndedListener();
        } catch (err) {
            var er = err.message;
        }
    },
    isCanOpen: function () {
        var isCanOpenVal = (codeReader !== undefined && codeReader !== null && codeReader.isMediaDevicesSuported === true) ? true : false;
        return isCanOpenVal;
    },
    open: function () {
        if (!isOpenCameraScan) {
            isOpenCameraScan = true;
            if (!$('.js_popup-scan').hasClass('active')) {
                $('.js_popup-scan').addClass('active');
                this.start();
            }
        }

    },
    close: function () {
        if (isOpenCameraScan) {
            isOpenCameraScan = false;
            $('.js_popup-scan').addClass('closed');

            setTimeout(function () {
                $('.js_popup-scan').removeClass('active');
                $('.js_popup-scan').removeClass('closed');
            }, 600);
            this.stop();
        }

    },
    reset: function () {
        if (isOpenCameraScan) {
            try {
                codeReader.reset();
                setTimeout(function () { cameraScanObj.start() }, 1000);
            } catch (err) {
                var er = err.message;
            }
        }
    },
    success: function () {
        if (!isSuccessCameraScan) {
            isSuccessCameraScan = true;
            setTimeout(function () {
                cameraScanObj.close();
            }, 1000);
        }
    }
};

window.commonObj = {
    setCookies: function (cookieName, cookieVal) {
        var updatedCookie = encodeURIComponent(cookieName) + "=" + encodeURIComponent(cookieVal) + ';path=/;';
        document.cookie = updatedCookie;
    },
    deleteCookies: function myfunction(cookieName) {
        this.setCookies(cookieName, "", {
            'max-age': -1
        });
    },
    correctFooterBtn: function () {
        correctWindow();
    }
};

window.qualityObj = {
    setFocusOnInput: function () {
        setTimeout(function () {
            try {
                if ($('html').hasClass('desktop')) {
                    document.getElementById("singleInput").focus();
                }
            } catch (e) {
                /*error*/
            }

        }, 1200);
    }
};

function blockedButtonCameraScan() {
    if (wx <= 667 && wy <= 400) {
        //if (!$('.js_popup-scan_show').hasClass('btn-disabled')) {
        //    $('.js_popup-scan_show').addClass('btn-disabled');
        //}
        $('.js_popup-scan_show').prop('disabled', true);
    } else {
        //$('.js_popup-scan_show').removeClass('btn-disabled');
        if (!$('.js_popup-scan_show').hasClass('btn_code')) {
            $('.js_popup-scan_show').prop('disabled', false);
        }


    }
}

function footerActionBar() {
    if (wx < 666 && $('.js_footer-buttons').children('.btn').length > 5) {
        isActiveFooterBtn = true;
        if (!$('.js_action-bar_show').hasClass('active')) {
            $('.js_action-bar_show').addClass('active');
        }
        if (!$('.js_footer-buttons').hasClass('active')) {
            $('.js_footer-buttons').addClass('active');
        }

    } else {
        isActiveFooterBtn = false;
        $('.js_action-bar_show').removeClass('active');
        $('.js_footer-buttons').removeClass('active');
    }
}

function correctWindow() {
    wx = window.innerWidth;
    wy = window.innerHeight;

    blockedButtonCameraScan();
    footerActionBar();
    focusSingleInput();
}


mql.addListener(function (m) {
    if (!m.matches) {
        if (isOpenCameraScan && isActiveFooterBtn) {
            cameraScanObj.close();
        } else if (wy < 440) {
            if (isOpenCameraScan) {
                cameraScanObj.close();
            }
        }
    }
});

function replaceCharByCode(keyCode, isShiftKey) {
    var code = '';
    try {
        if (isShiftKey) {
            if (keyCode > 64 && keyCode < 91) {
                code = charStandart.get(keyCode);
                code = code.toUpperCase();
            } else {
                code = charShift.get(keyCode);
            }
        } else {
            code = charStandart.get(keyCode);
        }
    } catch (err) {
        code = '';
    }
    return code;
}

function focusSingleInput() {
    if (isSingleInputFocus) {
        if (realHeight <= wy) {
            $('body').removeClass('mobile-footer-off');
            // isSingleInputFocus = false;
        } else {
            if (!$('html').hasClass('desktop') && !$('body').hasClass('mobile-footer-off')) {
                $('body').addClass('mobile-footer-off');
            }
        }
    }
}

// jQuery

$(window).resize(function () {
    correctWindow();
});

$('body').on('click', '.js_action-bar_show', function () {
    $('.js_action-bar_show').toggleClass('more')
    $('.js_footer-buttons').toggleClass('more');
});


$("*").on("keydown", "*", function (e) {
    if (!isSealedScanCode) {
        var input = $('#inputValue');
        if (input.length > 0
            && !input.is(':focus')
            && isBarCodeScan === false
            && isKeyDown === false) {
            if (e.keyCode !== 13) {
                if (e.shiftKey && e.keyCode > 0) {
                    isShift = true;
                }
                if (e.altKey && e.keyCode > 0) {
                    //
                } else if (
                    (e.keyCode === 9 || e.keyCode === 32)
                    || (e.keyCode > 47 && e.keyCode < 58)
                    || (e.keyCode > 64 && e.keyCode < 91)
                    || (e.keyCode > 95 && e.keyCode < 112)
                    || (e.keyCode > 185 && e.keyCode < 193)
                    || (e.keyCode > 218 && e.keyCode < 223)
                ) {

                    // scanBarCode += e.key.toString();
                    if (isUseEnglishKeyboard) {
                        scanBarCode += replaceCharByCode(e.keyCode, isShift);
                    } else {
                        scanBarCode += e.key.toString();
                    }
                    scanCode = '';
                    isKeyDown = true;
                    isShift = false;
                }
            } else {
                isBarCodeScan = true;
                isSealedScanCode = true;
                $('#barcodeInput').trigger('click');
            }
        }
    }
});

$("*").on("keyup", "*", function (e) {
    isKeyDown = false;
});

$("*").on("focus", ".btn-simualte_scan", function (e) {
    e.preventDefault();
});

$("*").on("click", "#btnModalSimulateScanCancel", function () {
    modalScanObj.cancelScanCode();
});

$("*").on("keydown", "#inputValue", function (e) {
    if (isSealedScanCode) {
        var inputVal = $('#inputValue');
        if (inputVal.length > 0 && inputVal.is(':focus')) {
            if (e.keyCode === 13) {
                scanCode = inputVal.val();
                $('#btnModalSimulateScan').trigger('click');
            }
        }
    }
});

$('*').on('focus input', '#singleInput,#inputValue', function () {
    if (!isSingleInputFocus) {
        if (!$('html').hasClass('desktop')) {
            realHeight = window.innerHeight;
            isSingleInputFocus = true;
            $('body').addClass('mobile-footer-off');
        }
    }

});

$('*').on('blur', '#singleInput,#inputValue', function () {
    $('body').removeClass('mobile-footer-off');
    isSingleInputFocus = false;
});

$('*').on("keydown", "#singleInput", function (e) {
    if (isKeyDown === false) {
        if (e.keyCode === 13) {
            $("#singleInput").blur();
            $('#btnSaveData').trigger('click');
            isKeyDown = true;
        }
    }

});

function initSlickSliderTasks() {
    /* if($('.slick-multiple-items')) {
        $('.slick-multiple-items').slick({
            infinite: false,
            slidesToShow: 15,
            slidesToScroll: 15,
            arrows: true,
            responsive: [
              {
                breakpoint: 1025,
                settings: {
                  slidesToShow: 15,
                  slidesToScroll: 15,
                }
              },
              {
                breakpoint: 769,
                settings: {
                  slidesToShow: 11,
                  slidesToScroll: 11,
                }
              },
              {
                breakpoint: 668,
                settings: {
                  slidesToShow: 9,
                  slidesToScroll: 9,
                }
              },
              {
                breakpoint: 561,
                settings: {
                  slidesToShow: 8,
                  slidesToScroll: 8,
                  arrows: false
                }
              },
              {
                breakpoint: 376,
                settings: {
                  slidesToShow: 6,
                  slidesToScroll: 6
                }
              }
              // You can unslick at a given breakpoint now by adding:
              // settings: "unslick"
              // instead of a settings object
            ]
          });
          $('.slick-multiple-items').css({'visibility': 'visible'})
    } */

    if($(".slick-multiple-items")) {
        $(".owl-carousel").owlCarousel({
            items: 3,
            margin: 0,
            dots: false,
            nav: true,
            responsive: {
                0: {
                    items: 3
                },
                376: {
                    items: 6
                },
                561: {
                    items: 8
                },
                668: {
                    items: 9
                },
                769: {
                    items: 11
                },
                1025: {
                    items: 15
                },
                1200: {
                    items: 18
                },
                1366: {
                    items: 20
                },
                1440: {
                    items: 22
                },
                1600: {
                    items: 24
                },
                1800: {
                    items: 27
                },
                1980: {
                    items: 31
                }
            }
        });
        $('.slick-multiple-items').css({'visibility': 'visible'})
    }
}

$(document).ready(function() {
    var windowHeight = window.innerHeight
    $('.body').css({height: windowHeight})
    // getHeightContentBlock()
    footerActionBar()

    initSlickSliderTasks()
  })